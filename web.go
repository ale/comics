package comics

import (
	"archive/zip"
	"bytes"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"html/template"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
	"time"

	"git.autistici.org/ale/comics/comicvine"
	"git.autistici.org/ale/comics/util"
	"github.com/dustin/go-humanize"
	"github.com/gorilla/mux"
	"github.com/varstr/uaparser"
)

var (
	htdocsDir = flag.String("htdocs", "./htdocs", "Directory with static content")

	tpl *template.Template
)

// Request context common to all templates.
type contextBase struct {
	// Value for the search field in the nav bar.
	SearchQuery string
}

type issueWithScore struct {
	*comicvine.Issue
	Score float64
}

type issueWithScoreList []issueWithScore

func (l issueWithScoreList) Len() int      { return len(l) }
func (l issueWithScoreList) Swap(i, j int) { l[i], l[j] = l[j], l[i] }
func (l issueWithScoreList) Less(i, j int) bool {
	// Sort scores in descending order.
	return l[i].Score > l[j].Score
}

type searchResultVolumeGroup struct {
	Volume   *comicvine.VolumeRef
	Issues   []issueWithScore
	TopScore float64
}

// Search results are mangled quite a lot: the issues found are
// grouped by volume, sorted by (descending) score. Volume groups are
// then sorted according to the top score of their issues.
type searchResultAggregate []*searchResultVolumeGroup

func (l searchResultAggregate) Len() int      { return len(l) }
func (l searchResultAggregate) Swap(i, j int) { l[i], l[j] = l[j], l[i] }
func (l searchResultAggregate) Less(i, j int) bool {
	// Sort scores in descending order.
	return l[i].TopScore > l[j].TopScore
}

// Group issues from a search result by volume. Returns the aggregate
// results, and the total number of issues found.
func (c *ComicsDB) aggregateSearchResults(s SearchResults) (searchResultAggregate, int) {
	var numres int
	volumes := make(map[int]*comicvine.VolumeRef)
	tmp := make(map[int][]issueWithScore)
	for _, r := range s.Results {
		// Find Issue from id.
		id, err := strconv.Atoi(r.Id)
		if err != nil {
			continue
		}
		issue, err := c.DB.GetIssue(id)
		if err != nil {
			continue
		}
		var volumeid int
		if issue.Volume != nil {
			volumeid = issue.Volume.Id
			volumes[volumeid] = issue.Volume
		}
		tmp[volumeid] = append(tmp[volumeid], issueWithScore{issue, r.Score})
		numres++
	}

	var result searchResultAggregate
	for volumeid, issues := range tmp {
		sort.Sort(issueWithScoreList(issues))
		var topScore float64
		for _, i := range issues {
			if i.Score > topScore {
				topScore = i.Score
			}
		}
		result = append(result, &searchResultVolumeGroup{
			Volume:   volumes[volumeid],
			Issues:   issues,
			TopScore: topScore,
		})
	}
	sort.Sort(result)
	return result, numres
}

// Handle a search request.
func (c *ComicsDB) searchHandler(w http.ResponseWriter, r *http.Request) {
	var ctx struct {
		contextBase
		Results    searchResultAggregate
		NumResults int
	}
	ctx.SearchQuery = r.FormValue("q")

	if ctx.SearchQuery != "" {
		reader, err := c.getIndexReader()
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		rr, err := reader.Search(ctx.SearchQuery, "issue")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		ctx.Results, ctx.NumResults = c.aggregateSearchResults(rr)
	}

	render("search.html", w, &ctx)
}

// Handle an autocompletion request. The response is JSON-encoded.
func (c *ComicsDB) suggestHandler(w http.ResponseWriter, r *http.Request) {
	query := r.FormValue("q")
	var suggestions []string
	if query != "" {
		reader, err := c.getIndexReader()
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		suggestions, err = reader.Suggest(query, "volume")
		if err != nil {
			log.Printf("suggest: %v", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(suggestions)
}

// Handler for file identification. When the autodetected metadata is
// wrong, the users can fix it by choosing among the various matches
// returned by the Comicvine API. If a better match is selected, the
// file is reparented to the new issue. The old issue may be removed
// later (with "comicsd verify") if orphaned.
//
// The page contains a few controls to let users refine the query that
// is sent to the Comicvine server, so we don't trigger reassignment
// until the "chosen_id" form field is set.
func (c *ComicsDB) reidentifyHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	md5 := vars["md5"]
	file, err := c.DB.GetFile(md5)
	if err != nil {
		http.NotFound(w, r)
		return
	}

	// If the user has picked a better choice, detach the file
	// from the old issue, fetch the new metadata if necessary,
	// and save the file again.
	if r.Method == "POST" {
		issueStr := r.FormValue("chosen_id")
		if issueStr != "" {
			issueId, err := strconv.Atoi(issueStr)
			if err != nil {
				http.Error(w, "Bad Request", http.StatusBadRequest)
				return
			}

			issue, err := c.vine.GetIssueById(issueId)
			if err != nil {
				log.Printf("error fetching issue %d: %v", issueId, err)
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			if err := c.SaveIssue(issue); err != nil {
				log.Printf("error saving issue %d: %v", issueId, err)
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			log.Printf("file %s is actually issue %d", md5, issueId)
			c.DB.DisassociateFileFromIssue(file, file.IssueId)
			file.IssueId = issueId
			c.DB.PutFile(file)

			http.Redirect(w, r, "/issue/"+issueStr, 302)
			return
		}
	}

	query := r.FormValue("q")
	if query == "" {
		query = util.SanitizeFileName(file.OriginalName)
	}

	issueNumber := r.FormValue("issue_number")
	if issueNumber != "" && !strings.Contains(query, "issue_number:") {
		query = fmt.Sprintf("%s AND issue_number:%s", query, issueNumber)
	}

	candidates, err := c.vine.SearchIssue(query, 20)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	ctx := struct {
		contextBase
		File        *File
		Query       string
		IssueNumber string
		Candidates  []*comicvine.Issue
	}{
		File:        file,
		Query:       query,
		IssueNumber: issueNumber,
		Candidates:  candidates,
	}
	render("reidentify.html", w, &ctx)
}

type issueList []*comicvine.Issue

func (l issueList) Len() int      { return len(l) }
func (l issueList) Swap(i, j int) { l[i], l[j] = l[j], l[i] }
func (l issueList) Less(i, j int) bool {
	inum, _ := strconv.Atoi(l[i].IssueNumber)
	jnum, _ := strconv.Atoi(l[j].IssueNumber)
	return inum < jnum
}

type issueWithFiles struct {
	*comicvine.Issue
	Files []*File
}

type volumeContext struct {
	contextBase
	Volume      *comicvine.Volume
	Issues      []issueWithFiles
	Description []template.HTML
	NumFiles    int
	TotalSize   int64
}

func (c *ComicsDB) getVolumeContext(w http.ResponseWriter, r *http.Request) (*volumeContext, error) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		http.NotFound(w, r)
		return nil, err
	}

	volume, err := c.DB.GetVolume(id)
	if err != nil {
		log.Printf("GetVolume(%d): %v", id, err)
		http.NotFound(w, r)
		return nil, err
	}

	// Fetch all the issues and their files (and group them
	// together in a struct).
	var numFiles int
	var totalSize int64
	var issuesAndFiles []issueWithFiles
	issues, _ := c.DB.GetVolumeIssues(id)
	sort.Sort(issueList(issues))
	for _, issue := range issues {
		i := issueWithFiles{Issue: issue}
		if files, err := c.DB.GetIssueFiles(issue.Id); err == nil {
			i.Files = files
			for _, f := range files {
				numFiles++
				totalSize += f.Size
			}
		}
		issuesAndFiles = append(issuesAndFiles, i)
	}

	return &volumeContext{
		Volume:      volume,
		Issues:      issuesAndFiles,
		Description: htmlParagraphs(volume.Description),
		NumFiles:    numFiles,
		TotalSize:   totalSize,
	}, nil
}

// Handler that shows the page for a specific volume.
func (c *ComicsDB) showVolumeHandler(w http.ResponseWriter, r *http.Request) {
	ctx, err := c.getVolumeContext(w, r)
	if err == nil {
		render("volume.html", w, ctx)
	}
}

// Handler to download all the issues in a volume as a single, huge
// ZIP file.
func (c *ComicsDB) downloadVolumeArchiveHandler(w http.ResponseWriter, r *http.Request) {
	ctx, err := c.getVolumeContext(w, r)
	if err != nil {
		return
	}

	w.Header().Set("Content-Type", "application/zip")
	w.Header().Set("Content-Disposition", fmt.Sprintf("attachment; filename=\"%s.zip\"", ctx.Volume.Name))

	zw := zip.NewWriter(w)
	for _, issue := range ctx.Issues {
		for _, file := range issue.Files {
			localf, err := c.files.Open(file.Path)
			if err != nil {
				log.Printf("zip: no local file for %+v", file)
				continue
			}

			f, err := zw.Create(filepath.Join(ctx.Volume.Name, filepath.Base(file.OriginalName)))
			if err != nil {
				log.Printf("zip: %v", err)
				localf.Close()
				return
			}
			io.Copy(f, localf)
			localf.Close()
		}
	}

	zw.Close()
}

// Handler that shows the page for a specific issue.
func (c *ComicsDB) showIssueHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		http.NotFound(w, r)
		return
	}

	issue, err := c.DB.GetIssue(id)
	if err != nil {
		log.Printf("GetIssue(%d): %v", id, err)
		http.NotFound(w, r)
		return
	}
	files, _ := c.DB.GetIssueFiles(id)

	ctx := struct {
		contextBase
		Issue       *comicvine.Issue
		Files       []*File
		Description []template.HTML
	}{
		Issue:       issue,
		Files:       files,
		Description: htmlParagraphs(issue.Description),
	}
	render("issue.html", w, &ctx)
}

var imageCacheClient = &http.Client{}

// Fetch an image from the remote Comicvine server.
func (c *ComicsDB) fetchIssueImage(id int, size string) (*Image, error) {
	// Since we're only given the issue ID, we need to retrieve
	// the Issue from the database in order to access its Image
	// field (that has the full image URL).
	issue, err := c.DB.GetIssue(id)
	if err != nil {
		return nil, err
	}
	if issue.Image == nil {
		return nil, errors.New("no image")
	}

	imgURL := issue.Image.PickSize(size)
	if imgURL == "" {
		return nil, errors.New("no image URL")
	}

	resp, err := imageCacheClient.Get(imgURL)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return nil, fmt.Errorf("HTTP status code %d", resp.StatusCode)
	}

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	return &Image{
		Data:     data,
		MimeType: resp.Header.Get("Content-Type"),
		Stamp:    time.Now().Unix(),
	}, nil
}

// Serve a cached image. If the image is not found in the local
// database, it is retrieved using its original URL.
func (c *ComicsDB) imageHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	size := vars["size"]
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		http.NotFound(w, r)
		return
	}

	img, err := c.DB.GetIssueImage(id, size)
	if err != nil {
		img, err = c.fetchIssueImage(id, size)
		if err != nil {
			log.Printf("fetchIssueImage(%d, %s): %v", id, size, err)
			http.NotFound(w, r)
			return
		}
		c.DB.PutIssueImage(id, size, img)
	}

	w.Header().Set("Content-Type", img.MimeType)
	w.Header().Set("Content-Length", strconv.Itoa(len(img.Data)))
	// Expire 1 year in the future, just to pollute people's
	// caches.
	expires := time.Now().Add(365 * 24 * time.Hour)
	w.Header().Set("Expires", expires.Format(http.TimeFormat))
	w.Write(img.Data)
}

// File check handler. Verifies if a specific file is already present
// in our database, using the file's MD5 fingerprint.
func (c *ComicsDB) checkHandler(w http.ResponseWriter, r *http.Request) {
	md5 := r.FormValue("md5")
	if !c.DB.HasFile(md5) {
		http.NotFound(w, r)
		return
	}
	// Empty 200 OK response.
}

// Upload handler. Receives multipart/form-data POST submissions from
// clients, containing one or more files. Submitted files should have
// their original file names (in the MIME headers), since that's what
// is used to autodetect metadata.
//
// Files are saved into a temporary location, and queued for
// processing in the "incoming" pipeline.
func (c *ComicsDB) uploadHandler(w http.ResponseWriter, r *http.Request) {
	// Process the request as multipart/form-data, extracting
	// information for each file from the MIME headers.
	mime, err := r.MultipartReader()
	if err != nil {
		log.Printf("upload: non-multipart request: %v", err)
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}

	for {
		// Advance to the next MIME part.
		part, err := mime.NextPart()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Printf("upload: %s: %v", r.RemoteAddr, err)
			http.Error(w, "Bad Request", http.StatusBadRequest)
			return
		}

		filename := part.FileName()
		if filename == "" {
			log.Printf("upload: empty filename in MIME part")
			continue
		}

		// Copy the input to a temporary file.
		tmpf, err := c.files.TempFile("comics_upload_")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		_, err = io.Copy(tmpf, part)
		tmpf.Close()
		if err != nil {
			os.Remove(tmpf.Name())
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		// Enqueue the new file.
		EnqueueJSON("incoming", &incomingFile{
			RemotePath: filename,
			LocalPath:  tmpf.Name(),
		})
	}
}

// Download handler. Simply serves the requested file from our static
// file repository.
func (c *ComicsDB) downloadHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	md5 := vars["md5"]
	file, err := c.DB.GetFile(md5)
	if err != nil {
		http.NotFound(w, r)
		return
	}
	issue, err := c.DB.GetIssue(file.IssueId)
	if err != nil {
		log.Printf("warning: file %s references non-existing issue %d", md5, file.IssueId)
		http.NotFound(w, r)
		return
	}

	fh, err := c.files.Open(file.Path)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer fh.Close()

	w.Header().Set("Content-Type", file.MimeType)
	w.Header().Set("Content-Length", strconv.FormatInt(file.Size, 10))
	w.Header().Set("Content-Disposition", fmt.Sprintf("attachment; filename=\"%s.cbz\"", issue.Filename()))
	io.Copy(w, fh)
}

// Serve the download binaries, if present.
func (c *ComicsDB) downloadClientHandler(w http.ResponseWriter, r *http.Request) {
	ua := uaparser.Parse(r.Header.Get("User-Agent"))
	var ospart, archpart string
	switch ua.OS.Name {
	case "Mac OS":
		ospart = "osx"
		archpart = "x86_64"
	case "Linux":
		ospart = "linux"
		archpart = "x86_64"
	}

	bindir := filepath.Join(*htdocsDir, "static", "dist")

	var downloadLink string
	if ospart != "" && archpart != "" {
		filename := fmt.Sprintf("comics-upload-%s-%s.gz", ospart, archpart)
		if _, err := os.Stat(filepath.Join(bindir, filename)); err == nil {
			downloadLink = "/static/dist/" + filename
		}
	}

	var availableFiles []string
	if glob, err := filepath.Glob(filepath.Join(bindir, "comics-upload-*.gz")); err == nil {
		for _, g := range glob {
			availableFiles = append(availableFiles, filepath.Base(g))
		}
	}

	ctx := struct {
		contextBase
		AvailableFiles []string
		UserAgent      string
		OS             string
		Arch           string
		UA             *uaparser.UAInfo
		DownloadLink   string
	}{
		AvailableFiles: availableFiles,
		UA:             ua,
		UserAgent:      r.Header.Get("User-Agent"),
		OS:             ospart,
		Arch:           archpart,
		DownloadLink:   downloadLink,
	}
	render("download.html", w, &ctx)
}

// Homepage handler.
func (c *ComicsDB) homeHandler(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "" && r.URL.Path != "/" {
		http.NotFound(w, r)
		return
	}

	render("index.html", w, nil)
}

func SetupHTTPServer(c *ComicsDB) {
	// Load templates.
	var err error
	tpl, err = template.New("comicsd").Funcs(template.FuncMap{
		"bytes":   humanizeBytes,
		"jsonify": jsonify,
	}).ParseGlob(filepath.Join(*htdocsDir, "templates", "*.html"))
	if err != nil {
		log.Fatalf("Couldn't load templates: %v", err)
	}

	// Serve static files.
	http.Handle("/static/", http.StripPrefix("/static/",
		http.FileServer(http.Dir(filepath.Join(*htdocsDir, "static")))))

	r := mux.NewRouter()
	r.HandleFunc("/volume/{id:[0-9]+}", c.showVolumeHandler)
	r.HandleFunc("/issue/{id:[0-9]+}/img/{size}", c.imageHandler)
	r.HandleFunc("/issue/{id:[0-9]+}", c.showIssueHandler)
	r.HandleFunc("/search", c.searchHandler)
	r.HandleFunc("/suggest", c.suggestHandler)
	r.HandleFunc("/upload", c.uploadHandler).Methods("POST")
	r.HandleFunc("/check", c.checkHandler)
	r.HandleFunc("/download", c.downloadClientHandler)
	r.HandleFunc("/dl_vol/{id:[0-9]+}", c.downloadVolumeArchiveHandler)
	r.HandleFunc("/dl/{md5:[a-f0-9]{32}}", c.downloadHandler).Methods("GET")
	r.HandleFunc("/reidentify/{md5:[a-f0-9]{32}}", c.reidentifyHandler)
	r.HandleFunc("/", c.homeHandler)

	http.Handle("/", r)
}

func render(templateName string, w http.ResponseWriter, ctx interface{}) {
	var buf bytes.Buffer
	if err := tpl.ExecuteTemplate(&buf, templateName, ctx); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	io.Copy(w, &buf)
}

func humanizeBytes(n int64) string {
	return humanize.Bytes(uint64(n))
}

func jsonify(obj interface{}) string {
	data, err := json.MarshalIndent(obj, "", "    ")
	if err != nil {
		return fmt.Sprintf("ERROR: %v", err)
	}
	return string(data)
}

func htmlParagraphs(s string) []template.HTML {
	var out []template.HTML
	for _, p := range util.SplitHTML(s) {
		out = append(out, template.HTML(p))
	}
	return out
}
