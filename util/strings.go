package util

import (
	"path/filepath"
	"regexp"
	"strings"
)

var (
	// Remove parenthesized expressions.
	parRx = regexp.MustCompile(`(\([^\)]*\)|\[[^\]]*\])`)

	// Remove common range suffixes (i.e. "chapter 1 of 19").
	ofRx = regexp.MustCompile(`(di|of|-|/)\s*[0-9]+`)

	// Remove leading zeros from numbers (001 -> 1).
	fixNumRx = regexp.MustCompile(`(^|[^0-9])0+(\d{1,2})($|[^0-9])`)

	// Remove any non-alphanumeric classes.
	noAlnumRx = regexp.MustCompile(`[^\pL\pN]+`)

	// Collapse whitespace.
	spcRx = regexp.MustCompile(`\s+`)

	// Split on HTML paragraph breaks.
	htmlParaRx = regexp.MustCompile(`<(/p|br/?)>`)

	// Remove HTML tags.
	unhtmlRx = regexp.MustCompile(`<[^>]*>`)

	// Elasticsearch special characters.
	esSpecialRx = regexp.MustCompile(`[-+":]+`)
)

// CollapseWhitespace collapses all whitespace into single spaces.
func CollapseWhitespace(s string) string {
	return strings.TrimSpace(spcRx.ReplaceAllString(s, " "))
}

// SanitizeFileName tries to extract only the "important" parts of a
// file name, returning something usable as a query string.
func SanitizeFileName(s string) string {
	s = strings.TrimSuffix(filepath.Base(s), filepath.Ext(s))
	s = parRx.ReplaceAllString(s, " ")
	s = ofRx.ReplaceAllString(s, " ")
	s = noAlnumRx.ReplaceAllString(s, " ")
	s = fixNumRx.ReplaceAllString(s, "$1$2$3")
	return CollapseWhitespace(s)
}

// SplitHTML mercilessly removes all HTML tags from the input and
// returns a list of plain text paragraphs.
func SplitHTML(s string) []string {
	var para []string
	for _, p := range htmlParaRx.Split(s, -1) {
		p = CollapseWhitespace(unhtmlRx.ReplaceAllString(p, " "))
		if p != "" {
			para = append(para, p)
		}
	}
	return para
}

// Convert HTML to plain text.
func HTMLToText(s string) string {
	return strings.Join(SplitHTML(s), "\n")
}

// Remove characters that are special to the Elasticsearch query
// syntax.
func RemoveESSpecialChars(s string) string {
	return CollapseWhitespace(esSpecialRx.ReplaceAllString(s, " "))
}
