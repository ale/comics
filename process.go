package comics

import (
	"encoding/json"
	"flag"
	"log"
	"net"
	"sync"

	"github.com/adjust/redismq"
)

var (
	redisAddr = flag.String("redis", "localhost:6379", "Redis address")
)

type Consumer interface {
	Process(payload string) error
}

type Worker struct {
	consumer Consumer
	name     string
	queue    *redismq.Queue
}

func NewWorker(name string, c Consumer) (*Worker, error) {
	queue, err := NewQueue(name)
	if err != nil {
		return nil, err
	}
	return &Worker{
		name:     name,
		queue:    queue,
		consumer: c,
	}, nil
}

func (w Worker) Run() error {
	consumer, err := w.queue.AddConsumer(w.name + "_consumer")
	if err != nil {
		return err
	}

	// Requeue unacked packages, which may have been left pending
	// by a previous crash.
	if err := consumer.RequeueWorking(); err != nil {
		return err
	}

	for {
		p, err := consumer.Get()
		if err != nil {
			log.Println(err)
			continue
		}
		if err := w.consumer.Process(p.Payload); err != nil {
			log.Printf("error: %v: %v", p.Payload, err)
			p.Fail()
		} else {
			p.Ack()
		}
	}
	return nil
}

var (
	queueCache     = map[string]*redismq.Queue{}
	queueCacheLock sync.Mutex
)

func NewQueue(name string) (*redismq.Queue, error) {
	queueCacheLock.Lock()
	defer queueCacheLock.Unlock()

	if q, ok := queueCache[name]; ok {
		return q, nil
	}

	host, port, err := net.SplitHostPort(*redisAddr)
	if err != nil {
		return nil, err
	}
	q := redismq.CreateQueue(host, port, "", 9, name)
	queueCache[name] = q
	return q, nil
}

func Enqueue(name, payload string) error {
	q, err := NewQueue(name)
	if err != nil {
		return nil
	}
	return q.Put(payload)
}

func EnqueueJSON(name string, obj interface{}) error {
	payload, err := json.Marshal(obj)
	if err != nil {
		return err
	}
	return Enqueue(name, string(payload))
}
