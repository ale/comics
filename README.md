
======
comics
======

Manage your comics collection in a centralized location, with rich metadata
and search capabilities.

## Requirements

 * a Comicvine API key (http://api.comicvine.com/)
 * Redis
 * Elasticsearch

and of course, you'll need the Go build environment to actually build
the software from source.


