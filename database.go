package comics

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"

	"git.autistici.org/ale/comics/comicvine"
	"git.autistici.org/ale/comics/util"
	"github.com/jmhodges/levigo"
)

// File uploaded by a user.
type File struct {
	// The hex-encoded MD5 hash of the file contents is used as
	// the unique ID.
	MD5 string

	// Path is the relative path of the file in the local storage.
	Path string

	// OriginalName is the original file name.
	OriginalName string

	// Many-to-one link to an issue.
	IssueId int

	MimeType string
	Size     int64
	Stamp    int64
}

// Image cached from Comicvine.
type Image struct {
	Data     []byte
	MimeType string
	Stamp    int64
}

// Content-addressed local storage. Paths are relative to the root (so
// that the storage can be moved on the filesystem).
type FileDB struct {
	Root string
}

func NewFileDB(dir string) *FileDB {
	return &FileDB{Root: dir}
}

// Create a temporary file in the local storage directory.
func (f FileDB) TempFile(prefix string) (*os.File, error) {
	tmpdir := filepath.Join(f.Root, "tmp")
	if _, err := os.Stat(tmpdir); err != nil {
		os.MkdirAll(tmpdir, 0700)
	}
	return ioutil.TempFile(tmpdir, prefix)
}

// Move a file to the local storage, return its unique ID and the
// relative path where it was stored.
func (f FileDB) Move(path string) (string, string, error) {
	md5, err := util.GetFileMD5(path)
	if err != nil {
		return "", "", err
	}

	// Hash local directories, one level deep.
	dir := md5[0:1]
	os.MkdirAll(filepath.Join(f.Root, dir), 0700)
	relpath := filepath.Join(dir, md5)
	if err := os.Rename(path, filepath.Join(f.Root, relpath)); err != nil {
		return "", "", err
	}
	return relpath, md5, nil
}

// Open a file given its relative path.
func (f FileDB) Open(relpath string) (*os.File, error) {
	return os.Open(filepath.Join(f.Root, relpath))
}

type DB struct {
	db     *levigo.DB
	cache  *levigo.Cache
	filter *levigo.FilterPolicy
}

func NewDB(path string) (*DB, error) {
	opts := levigo.NewOptions()
	cache := levigo.NewLRUCache(2 << 28)
	opts.SetCache(cache)
	opts.SetCreateIfMissing(true)
	filter := levigo.NewBloomFilter(10)
	opts.SetFilterPolicy(filter)
	db, err := levigo.Open(path, opts)
	if err != nil {
		return nil, err
	}
	return &DB{db, cache, filter}, nil
}

func (l *DB) Close() {
	l.db.Close()
	l.cache.Close()
	l.filter.Close()
}

func (l *DB) get(key []byte) ([]byte, error) {
	ro := levigo.NewReadOptions()
	defer ro.Close()
	return l.db.Get(ro, key)
}

func (l *DB) getJson(key []byte, out interface{}) error {
	data, err := l.get(key)
	if err != nil {
		return nil
	}
	return json.Unmarshal(data, out)
}

func (l *DB) GetFile(md5 string) (*File, error) {
	var file File
	if err := l.getJson(keyWithStrId("file", md5), &file); err != nil {
		return nil, err
	}
	return &file, nil
}

func (l *DB) HasFile(md5 string) bool {
	_, err := l.GetFile(md5)
	return err == nil
}

func (l *DB) PutFile(file *File) error {
	wo := levigo.NewWriteOptions()
	defer wo.Close()

	data, err := json.Marshal(file)
	if err != nil {
		return err
	}

	wb := levigo.NewWriteBatch()
	wb.Put(keyWithStrId("file", file.MD5), data)
	wb.Put(keyWithStrIds("issue/files", file.IssueId, file.MD5), []byte(file.MD5))
	return l.db.Write(wo, wb)
}

func (l *DB) DisassociateFileFromIssue(file *File, id int) error {
	wo := levigo.NewWriteOptions()
	defer wo.Close()
	return l.db.Delete(wo, keyWithStrIds("issue/files", id, file.MD5))
}

func (l *DB) GetVolume(id int) (*comicvine.Volume, error) {
	var vol comicvine.Volume
	if err := l.getJson(keyWithId("volume", id), &vol); err != nil {
		return nil, err
	}
	return &vol, nil
}

func (l *DB) GetVolumeIssues(id int) ([]*comicvine.Issue, error) {
	start, end := keyRange(keyWithId("volume/issues", id))
	ro := levigo.NewReadOptions()
	defer ro.Close()
	it := l.db.NewIterator(ro)
	defer it.Close()
	it.Seek(start)

	var result []*comicvine.Issue
	for it.Seek(start); it.Valid() && bytes.Compare(it.Key(), end) < 0; it.Next() {
		id, _ := strconv.Atoi(string(it.Value()))
		if issue, err := l.GetIssue(id); err == nil {
			result = append(result, issue)
		}
	}
	return result, nil
}

func (l *DB) GetAllVolumes() ([]*comicvine.Volume, error) {
	start, end := keyNumRange([]byte("volume"))
	ro := levigo.NewReadOptions()
	defer ro.Close()
	it := l.db.NewIterator(ro)
	defer it.Close()
	it.Seek(start)

	var result []*comicvine.Volume
	for it.Seek(start); it.Valid() && bytes.Compare(it.Key(), end) < 0; it.Next() {
		var vol comicvine.Volume
		if err := json.Unmarshal(it.Value(), &vol); err == nil {
			result = append(result, &vol)
		}
	}
	return result, nil
}

func (l *DB) HasVolume(id int) bool {
	_, err := l.GetVolume(id)
	return err == nil
}

func (l *DB) PutVolume(vol *comicvine.Volume) error {
	data, err := json.Marshal(vol)
	if err != nil {
		return err
	}
	wo := levigo.NewWriteOptions()
	defer wo.Close()
	return l.db.Put(wo, keyWithId("volume", vol.Id), data)
}

func (l *DB) DeleteVolume(id int) error {
	wo := levigo.NewWriteOptions()
	defer wo.Close()
	return l.db.Delete(wo, keyWithId("volume", id))
}

func (l *DB) GetIssue(id int) (*comicvine.Issue, error) {
	var issue comicvine.Issue
	if err := l.getJson(keyWithId("issue", id), &issue); err != nil {
		return nil, err
	}
	return &issue, nil
}

func (l *DB) GetIssueFiles(id int) ([]*File, error) {
	start, end := keyRange(keyWithId("issue/files", id))
	ro := levigo.NewReadOptions()
	defer ro.Close()
	it := l.db.NewIterator(ro)
	defer it.Close()
	it.Seek(start)

	var result []*File
	for it.Seek(start); it.Valid() && bytes.Compare(it.Key(), end) < 0; it.Next() {
		md5 := string(it.Value())
		if issue, err := l.GetFile(md5); err == nil {
			result = append(result, issue)
		}
	}
	return result, nil
}

func (l *DB) GetIssueImage(id int, size string) (*Image, error) {
	ro := levigo.NewReadOptions()
	defer ro.Close()
	var image Image
	if err := l.getJson(keyWithStrIds("issue/images", id, size), &image); err != nil {
		return nil, err
	}
	return &image, nil
}

func (l *DB) PutIssueImage(id int, size string, img *Image) error {
	data, err := json.Marshal(img)
	if err != nil {
		return err
	}
	wo := levigo.NewWriteOptions()
	defer wo.Close()
	return l.db.Put(wo, keyWithStrIds("issue/images", id, size), data)
}

func (l *DB) GetAllIssues() ([]*comicvine.Issue, error) {
	start, end := keyNumRange([]byte("issue"))
	ro := levigo.NewReadOptions()
	defer ro.Close()
	it := l.db.NewIterator(ro)
	defer it.Close()
	it.Seek(start)

	var result []*comicvine.Issue
	for it.Seek(start); it.Valid() && bytes.Compare(it.Key(), end) < 0; it.Next() {
		var issue comicvine.Issue
		if err := json.Unmarshal(it.Value(), &issue); err == nil {
			result = append(result, &issue)
		}
	}
	return result, nil
}

func (l *DB) HasIssue(id int) bool {
	_, err := l.GetIssue(id)
	return err == nil
}

func (l *DB) PutIssue(issue *comicvine.Issue) error {
	data, err := json.Marshal(issue)
	if err != nil {
		return err
	}

	wo := levigo.NewWriteOptions()
	defer wo.Close()

	wb := levigo.NewWriteBatch()
	wb.Put(keyWithId("issue", issue.Id), data)
	if issue.Volume != nil {
		wb.Put(keyWithStrIds("volume/issues", issue.Volume.Id, issue.IssueNumber), []byte(strconv.Itoa(issue.Id)))
	}

	return l.db.Write(wo, wb)
}

func (l *DB) DeleteIssue(id int) error {
	wb := levigo.NewWriteBatch()
	wb.Delete(keyWithId("issue", id))

	// Delete associated resources.
	ro := levigo.NewReadOptions()
	defer ro.Close()
	for _, prefix := range []string{"issue/files", "issue/images"} {
		start, end := keyRange(keyWithId(prefix, id))
		it := l.db.NewIterator(ro)
		it.Seek(start)
		for it.Seek(start); it.Valid() && bytes.Compare(it.Key(), end) < 0; it.Next() {
			wb.Delete(it.Key())
		}
		it.Close()
	}

	wo := levigo.NewWriteOptions()
	defer wo.Close()
	return l.db.Write(wo, wb)
}

func keyWithId(bucket string, id int) []byte {
	return []byte(fmt.Sprintf("%s/%d", bucket, id))
}

func keyWithIds(bucket string, id1, id2 int) []byte {
	return []byte(fmt.Sprintf("%s/%d/%d", bucket, id1, id2))
}

func keyWithStrId(bucket string, id string) []byte {
	return []byte(fmt.Sprintf("%s/%s", bucket, id))
}

func keyWithStrIds(bucket string, id1 int, id2 string) []byte {
	return []byte(fmt.Sprintf("%s/%d/%s", bucket, id1, id2))
}

func keyRange(prefix []byte) ([]byte, []byte) {
	start := make([]byte, len(prefix)+1)
	end := make([]byte, len(prefix)+1)
	copy(start, prefix)
	copy(end, prefix)
	start[len(prefix)] = byte('/')
	end[len(prefix)] = byte('/') + 1
	return start, end
}

func keyNumRange(prefix []byte) ([]byte, []byte) {
	start := make([]byte, len(prefix)+2)
	end := make([]byte, len(prefix)+2)
	copy(start, prefix)
	copy(end, prefix)
	start[len(prefix)] = byte('/')
	start[len(prefix)+1] = byte('0')
	end[len(prefix)] = byte('/')
	end[len(prefix)+1] = byte('9') + 1
	return start, end
}
