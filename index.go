package comics

import (
	"flag"
	"log"
	"net"
	"net/url"
	"strconv"
	"strings"

	"git.autistici.org/ale/comics/third_party/goes"
)

var esAddr = flag.String("es", "localhost:9200", "ElasticSearch address")

const comicsIndex = "comics"

type Index struct {
	conn *goes.Connection
}

func NewIndex() (*Index, error) {
	host, port, err := net.SplitHostPort(*esAddr)
	if err != nil {
		return nil, err
	}
	conn := goes.NewConnection(host, port)
	idx := &Index{conn}
	idx.createIndex()
	return idx, nil
}

func (l *Index) createIndex() {
	mapping := map[string]interface{}{
		"settings": map[string]interface{}{
			"index.number_of_shards":   5,
			"index.number_of_replicas": 1,
		},
		"mappings": map[string]interface{}{
			"_default_": map[string]interface{}{
				"_source": map[string]interface{}{
					"enabled": false,
				},
				"_all": map[string]interface{}{
					"enabled": true,
				},
				"properties": map[string]interface{}{
					"description": map[string]interface{}{
						"type":  "string",
						"store": false,
					},
				},
			},
			"volume": map[string]interface{}{
				"properties": map[string]interface{}{
					"name": map[string]interface{}{
						"type": "string",
					},
					"suggest": map[string]interface{}{
						"type":                         "completion",
						"index_analyzer":               "simple",
						"search_analyzer":              "simple",
						"payloads":                     true,
						"preserve_position_increments": false,
					},
				},
			},
		},
	}
	_, err := l.conn.CreateIndex(comicsIndex, mapping)
	if err != nil {
		if serr, ok := err.(*goes.SearchError); !ok || !strings.HasPrefix(serr.Msg, "IndexAlreadyExistsException") {
			log.Printf("createindex: %v", err)
		}
	}
}

func (l *Index) ClearIndex() {
	l.conn.DeleteIndex(comicsIndex)
	l.createIndex()
}

func (l *Index) Close() {}

type IndexUpdater struct {
	*Index
}

func NewIndexUpdater(base *Index) (*IndexUpdater, error) {
	return &IndexUpdater{base}, nil
}

func (u *IndexUpdater) Close() {}

type IndexDocument interface {
	Id() int
	Type() string
	Fields() map[string]string
	Suggest() map[string]interface{}
}

func (u *IndexUpdater) Add(doc IndexDocument) error {
	// Build the ES document.
	esdoc := goes.Document{
		Index:  comicsIndex,
		Type:   doc.Type(),
		Id:     strconv.Itoa(doc.Id()),
		Fields: map[string]interface{}{},
	}
	for k, v := range doc.Fields() {
		esdoc.Fields[k] = v
	}
	if s := doc.Suggest(); s != nil {
		esdoc.Fields["suggest"] = s
	}

	// Add it to the index.
	if _, err := u.conn.Index(esdoc, make(url.Values)); err != nil {
		return err
	}
	return nil
}

func (u *IndexUpdater) Delete(qtype string, id int) error {
	esdoc := goes.Document{
		Index: comicsIndex,
		Type:  qtype,
		Id:    strconv.Itoa(id),
	}
	if _, err := u.conn.Delete(esdoc, make(url.Values)); err != nil {
		return err
	}
	return nil
}

type SearchResults struct {
	NumResults uint
	Results    []goes.Hit
}

type IndexReader struct {
	*Index
}

func NewIndexReader(base *Index) (*IndexReader, error) {
	return &IndexReader{base}, nil
}

func (r *IndexReader) Close() {}

func (r *IndexReader) Search(query, queryType string) (SearchResults, error) {
	qmap := map[string]interface{}{
		"query": map[string]interface{}{
			"query_string": map[string]interface{}{
				"query": query,
			},
		},
	}

	var results SearchResults
	resp, err := r.conn.Search(qmap, []string{comicsIndex}, []string{queryType}, make(url.Values))
	if err != nil {
		return results, err
	}
	results.NumResults = uint(resp.Hits.Total)
	results.Results = resp.Hits.Hits
	return results, nil
}

func (r *IndexReader) Suggest(query, queryType string) ([]string, error) {
	qmap := map[string]interface{}{
		"text": query,
		"completion": map[string]interface{}{
			"field": "suggest",
			"fuzzy": map[string]interface{}{
				"fuzziness": 2,
			},
		},
	}

	resp, err := r.conn.Suggest(qmap, []string{comicsIndex}, nil, make(url.Values))

	if err != nil {
		return nil, err
	}

	results := []string{}
	for _, e := range resp.Suggest {
		for _, o := range e.Options {
			results = append(results, o.Text)
		}
	}

	return results, nil
}
