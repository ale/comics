package main

import (
	"bytes"
	"flag"
	"fmt"
	"io"
	"log"
	"mime/multipart"
	"net/http"
	"os"
	"strings"

	"git.autistici.org/ale/comics/util"
)

var (
	serverUrl = flag.String("server", "http://localhost:4004", "local server")
	dir       = flag.String("dir", ".", "directory to scan")
)

func uploadCollection(rootdir string, cc *ComicsClient) {
	// Scan the local collection, check and upload.
	w := util.NewDefaultWalker()
	w.Walk(rootdir, func(path string, info os.FileInfo, err error) error {
		md5, err := util.GetFileMD5(path)
		if err != nil {
			log.Printf("%s: error: %v", path, err)
			return nil
		}

		if ok, err := cc.CheckMD5(md5); err == nil && ok {
			// File already present (or error).
			log.Printf("%s: already present in the db", path)
			return nil
		} else if err != nil {
			log.Printf("server error: %v", err)
			return nil
		}

		log.Printf("uploading %s", path)
		if err := cc.Upload(path); err != nil {
			log.Printf("%s: upload failed: %v", path, err)
		}

		return nil
	})
}

type ComicsClient struct {
	client *http.Client
	uri    string
}

func NewComicsClient(uri string) *ComicsClient {
	return &ComicsClient{
		client: &http.Client{},
		uri:    strings.TrimRight(uri, "/"),
	}
}

func (c *ComicsClient) Upload(path string) error {
	file, err := os.Open(path)
	if err != nil {
		return err
	}
	defer file.Close()

	var buf bytes.Buffer
	writer := multipart.NewWriter(&buf)
	part, err := writer.CreateFormFile("file", path)
	if err != nil {
		return err
	}
	if _, err := io.Copy(part, file); err != nil {
		return err
	}
	if err := writer.Close(); err != nil {
		return err
	}

	req, err := http.NewRequest("POST", c.uri+"/upload", &buf)
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", writer.FormDataContentType())
	resp, err := c.client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return fmt.Errorf("HTTP status code %d", resp.StatusCode)
	}
	return nil
}

func (c *ComicsClient) CheckMD5(md5 string) (bool, error) {
	resp, err := c.client.Get(c.uri + "/check?md5=" + md5)
	if err != nil {
		return false, err
	}
	defer resp.Body.Close()
	return resp.StatusCode == 200, nil
}

func main() {
	flag.Parse()

	cc := NewComicsClient(*serverUrl)
	uploadCollection(*dir, cc)
}
