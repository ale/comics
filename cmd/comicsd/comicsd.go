package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"

	"git.autistici.org/ale/comics"
)

var (
	addr    = flag.String("addr", ":4004", "bind address")
	apiKey  = flag.String("api-key", "", "Comicvine API key")
	dataDir = flag.String("data-dir", ".", "data directory")
)

func verify(cdb *comics.ComicsDB) {
	refVolumes := make(map[int]struct{})
	u, err := comics.NewIndexUpdater(cdb.Index)
	if err != nil {
		log.Fatal(err)
	}
	defer u.Close()

	// Remove issues with no associated files.
	issues, err := cdb.DB.GetAllIssues()
	if err != nil {
		log.Fatal(err)
	}
	for _, issue := range issues {
		files, err := cdb.DB.GetIssueFiles(issue.Id)
		if err != nil {
			continue
		}
		if len(files) == 0 {
			log.Printf("issue %d has no files, removing...", issue.Id)
			cdb.DB.DeleteIssue(issue.Id)
			u.Delete("issue", issue.Id)
		} else {
			if issue.Volume != nil {
				refVolumes[issue.Volume.Id] = struct{}{}
			}
		}
	}

	// Remove volumes with no associated issues.
	volumes, err := cdb.DB.GetAllVolumes()
	if err != nil {
		log.Fatal(err)
	}
	for _, vol := range volumes {
		if _, ok := refVolumes[vol.Id]; !ok {
			log.Printf("volume %d has no issues, removing...", vol.Id)
			cdb.DB.DeleteVolume(vol.Id)
			u.Delete("volume", vol.Id)
		}
	}
}

func reindex(cdb *comics.ComicsDB) {
	issues, err := cdb.DB.GetAllIssues()
	if err != nil {
		log.Fatal(err)
	}

	volumes, err := cdb.DB.GetAllVolumes()
	if err != nil {
		log.Fatal(err)
	}

	cdb.Index.ClearIndex()

	for _, issue := range issues {
		log.Printf("indexing issue %d", issue.Id)
		cdb.IndexIssue(issue)
	}
	for _, vol := range volumes {
		log.Printf("indexing volume %d", vol.Id)
		cdb.IndexVolume(vol)
	}
}

func server(cdb *comics.ComicsDB) {
	comics.SetupHTTPServer(cdb)
	cdb.StartBackgroundJobs()
	log.Printf("starting HTTP server on %s", *addr)
	log.Fatal(http.ListenAndServe(*addr, nil))
}

func main() {
	flag.Parse()

	var cmd func(*comics.ComicsDB)
	switch flag.Arg(0) {
	case "server":
		cmd = server
	case "reindex":
		cmd = reindex
	case "verify":
		cmd = verify
	default:
		fmt.Fprintf(os.Stderr, `Usage: comicsd [<OPTIONS>] <COMMAND>

Known commands:

  server     Start the main web application
  reindex    Reindex all documents
  verify     Run a thorough database check

Run with --help to see the available options.

`)
		return
	}

	cdb, err := comics.NewComicsDB(*dataDir, *apiKey)
	if err != nil {
		log.Fatal(err)
	}
	defer cdb.Close()

	cmd(cdb)
}
