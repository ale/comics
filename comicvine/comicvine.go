package comicvine

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"
)

const apiEndpointUrl = "http://www.comicvine.com/api/"

type Publisher struct {
	Id           int
	Name         string
	ApiDetailURL string `json:"api_detail_url"`
}

type IssueRef struct {
	Id           int
	Name         string
	IssueNumber  string `json:"issue_number"`
	ApiDetailURL string `json:"api_detail_url"`
}

type Credits struct {
	Id           int
	Name         string
	Role         string
	APIDetailURL string `json:"site_detail_url"`
}

type Issue struct {
	IssueRef
	Image         *Image
	CoverDate     string `json:"cover_date"`
	DateAdded     string `json:"date_added"`
	Deck          string
	Description   string
	PersonCredits []*Credits `json:"person_credits"`
	Volume        *VolumeRef
}

// Filename suggested for download.
func (i Issue) Filename() string {
	var parts []string
	if i.Volume != nil {
		parts = append(parts, i.Volume.Name)
	}
	if i.IssueNumber != "" {
		parts = append(parts, i.IssueNumber)
	}
	if i.Name != "" {
		parts = append(parts, i.Name)
	}
	return strings.Join(parts, " - ")
}

type Image struct {
	Tiny   string `json:"tiny_url"`
	Thumb  string `json:"thumb_url"`
	Super  string `json:"super_url"`
	Small  string `json:"small_url"`
	Screen string `json:"screen_url"`
	Medium string `json:"medium_url"`
	Icon   string `json:"icon_url"`
}

func (i Image) PickSize(size string) string {
	switch size {
	case "tiny":
		return i.Tiny
	case "small":
		return i.Small
	case "medium":
		return i.Medium
	case "super":
		return i.Super
	case "thumb":
		return i.Thumb
	case "screen":
		return i.Screen
	case "icon":
		return i.Icon
	}
	return i.Small
}

type VolumeRef struct {
	Id           int
	Name         string
	ApiDetailURL string `json:"api_detail_url"`
}

type Volume struct {
	VolumeRef
	Publisher     *Publisher
	Image         *Image
	StartYear     string    `json:"start_year"`
	CountOfIssues int       `json:"count_of_issues"`
	FirstIssue    *IssueRef `json:"first_issue"`
	LastIssue     *IssueRef `json:"last_issue"`
	DateAdded     string    `json:"date_added"`
	Deck          string
	Description   string
}

// API response container.
type responseBase struct {
	Version         string
	StatusCode      int `json:"status_code"`
	Error           string
	NumTotalResults int `json:"number_of_total_results"`
	NumPageResults  int `json:"number_of_page_results"`
	Offset          int
	Limit           int
}

var errRateLimitExceeded = errors.New("rate limit exceeded")

func (r responseBase) Err() error {
	if r.StatusCode == 1 {
		return nil
	} else if r.StatusCode == 107 {
		return errRateLimitExceeded
	}
	return fmt.Errorf("Comicvine API error: %s (%d)", r.Error, r.StatusCode)
}

// Comicvine API client.
type Client struct {
	ApiKey string
	client *http.Client
}

func New(apikey string) *Client {
	return &Client{
		ApiKey: apikey,
		client: &http.Client{},
	}
}

type hasErr interface {
	Err() error
}

func (c Client) httpGet(uri string, out interface{}, args map[string]string) error {
	v := make(url.Values)
	v.Set("api_key", c.ApiKey)
	v.Set("format", "json")
	for arg, value := range args {
		v.Set(arg, value)
	}
	uri = uri + "?" + v.Encode()

	backoff := 2 * time.Second
	for {
		r, err := c.client.Get(uri)
		if err != nil {
			return err
		}
		if r.StatusCode != 200 {
			r.Body.Close()
			return fmt.Errorf("HTTP status code %d", r.StatusCode)
		}
		err = json.NewDecoder(r.Body).Decode(out)
		r.Body.Close()
		if err != nil {
			return err
		}
		responseWithErr := out.(hasErr)
		if err := responseWithErr.Err(); err != nil {
			if err == errRateLimitExceeded {
				log.Printf("Comicvine API rate limit exceeded, sleeping %g seconds...", backoff.Seconds())
				time.Sleep(backoff)
				backoff = backoff * 2
				continue
			}
			return err
		}
		return nil
	}
}

type volumeSearchResponse struct {
	responseBase
	Results []*Volume
}

func (c Client) SearchVolume(query string, limit int) ([]*Volume, error) {
	var resp volumeSearchResponse
	if err := c.httpGet(apiEndpointUrl+"search/", &resp, map[string]string{
		"query":     query,
		"limit":     strconv.Itoa(limit),
		"resources": "volume"}); err != nil {
		return nil, err
	}
	return resp.Results, nil
}

type issueSearchResponse struct {
	responseBase
	Results []*Issue
}

func (c Client) SearchIssue(query string, limit int) ([]*Issue, error) {
	var resp issueSearchResponse
	if err := c.httpGet(apiEndpointUrl+"search/", &resp, map[string]string{
		"query":     query,
		"limit":     strconv.Itoa(limit),
		"resources": "issue"}); err != nil {
		return nil, err
	}
	return resp.Results, nil
}

type volumeResponse struct {
	responseBase
	Results *Volume
}

func (c Client) GetVolume(ref *VolumeRef) (*Volume, error) {
	var resp volumeResponse
	if err := c.httpGet(ref.ApiDetailURL, &resp, nil); err != nil {
		return nil, err
	}
	return resp.Results, nil
}

type issueResponse struct {
	responseBase
	Results *Issue
}

func (c Client) GetIssue(ref *IssueRef) (*Issue, error) {
	var resp issueResponse
	if err := c.httpGet(ref.ApiDetailURL, &resp, nil); err != nil {
		return nil, err
	}
	return resp.Results, nil
}

func (c Client) GetIssueById(id int) (*Issue, error) {
	var resp issueSearchResponse
	if err := c.httpGet(apiEndpointUrl+"issues/", &resp, map[string]string{
		"filter": fmt.Sprintf("id:%d", id),
		"limit":  "1",
	}); err != nil {
		return nil, err
	}
	if len(resp.Results) == 0 {
		return nil, errors.New("not found")
	}
	return resp.Results[0], nil
}
