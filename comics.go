package comics

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"os"
	"path/filepath"

	"sync"
	"time"

	"git.autistici.org/ale/comics/comicvine"
	"git.autistici.org/ale/comics/util"
)

type ComicsDB struct {
	DB          *DB
	Index       *Index
	files       *FileDB
	vine        *comicvine.Client
	updater     *IndexUpdater
	updaterOnce sync.Once
	reader      *IndexReader
	readerOnce  sync.Once
}

func NewComicsDB(dir, apikey string) (*ComicsDB, error) {
	if err := os.MkdirAll(dir, 0700); err != nil {
		return nil, err
	}

	db, err := NewDB(filepath.Join(dir, "db"))
	if err != nil {
		return nil, err
	}

	//index, err := NewIndex(filepath.Join(dir, "index"), "en")
	index, err := NewIndex()
	if err != nil {
		return nil, err
	}

	return &ComicsDB{
		DB:    db,
		Index: index,
		vine:  comicvine.New(apikey),
		files: NewFileDB(filepath.Join(dir, "files")),
	}, nil
}

func (c *ComicsDB) Close() {
	if c.updater != nil {
		c.updater.Close()
	}
	if c.reader != nil {
		c.reader.Close()
	}
	c.Index.Close()
	c.DB.Close()
}

func (c *ComicsDB) Identify(filename string) (*comicvine.Issue, error) {
	results, err := c.vine.SearchIssue(util.SanitizeFileName(filename), 1)
	if err != nil {
		return nil, err
	}
	if len(results) == 0 {
		return nil, errors.New("no results")
	}
	return results[0], nil
}

func (c *ComicsDB) ProcessFile(incoming *incomingFile) (*comicvine.Issue, error) {
	// First of all, check if the local file is still there.
	stat, err := os.Stat(incoming.LocalPath)
	if err != nil {
		return nil, fmt.Errorf("local file has disappeared")
	}

	// See if we can figure out what issue this corresponds to
	// (using the filename). If nothing is found, simply discard
	// the file. The temporary local copy will be removed at the
	// end in all cases.
	defer os.Remove(incoming.LocalPath)
	issue, err := c.Identify(incoming.RemotePath)
	if err != nil {
		log.Printf("Could not identify file %s", filepath.Base(incoming.RemotePath))
		return nil, fmt.Errorf("could not identify issue: %v", err)
	}
	log.Printf("%s identified as %s / %s", filepath.Base(incoming.RemotePath), issue.Name, issue.Volume.Name)

	// The file matches an issue, so store everything away: the
	// file must be moved into local storage (obtaining a
	// permanent filename).
	dstfile, md5, err := c.files.Move(incoming.LocalPath)
	if err != nil {
		return nil, fmt.Errorf("could not copy file into local storage: %v", err)
	}

	if err := c.SaveIssue(issue); err != nil {
		return nil, err
	}

	// Create the File and store it in the database, associating
	// it with the issue detected above.
	file := &File{
		MD5:          md5,
		Path:         dstfile,
		OriginalName: filepath.Base(incoming.RemotePath),
		MimeType:     "application/zip",
		Size:         stat.Size(),
		Stamp:        time.Now().Unix(),
		IssueId:      issue.Id,
	}
	c.DB.PutFile(file)

	return issue, nil
}

// SaveIssue ensures that the issue and volume metadata are stored in
// our db (possibly fetching them from the Comicvine API server).
func (c *ComicsDB) SaveIssue(issue *comicvine.Issue) error {
	// If the issue is not in the database, retrieve it from the
	// remote service and store it. Same for the volume.
	if !c.DB.HasIssue(issue.Id) {
		apiIssue, err := c.vine.GetIssue(&issue.IssueRef)
		if err != nil {
			return fmt.Errorf("could not fetch issue information: %v", err)
		}
		c.DB.PutIssue(apiIssue)
		EnqueueJSON("index_issue", apiIssue)
	}
	if issue.Volume != nil {
		if !c.DB.HasVolume(issue.Volume.Id) {
			apiVolume, err := c.vine.GetVolume(issue.Volume)
			if err != nil {
				return fmt.Errorf("could not fetch volume information: %v", err)
			}
			c.DB.PutVolume(apiVolume)
			EnqueueJSON("index_volume", apiVolume)
		}
	}
	return nil
}

// Adapt an Issue to the IndexDocument interface.
type issueIndexDocument struct {
	*comicvine.Issue
}

func (d issueIndexDocument) Fields() map[string]string {
	fields := make(map[string]string)
	if d.Name != "" {
		fields["name"] = d.Name
	}
	if d.IssueNumber != "" {
		fields["issue_number"] = d.IssueNumber
	}
	if d.Volume != nil {
		fields["volume"] = d.Volume.Name
	}
	if d.Description != "" {
		fields["description"] = util.HTMLToText(d.Description)
	}
	return fields
}

func (d issueIndexDocument) Id() int {
	return d.Issue.Id
}

func (d issueIndexDocument) Type() string {
	return "issue"
}

func (d issueIndexDocument) Suggest() map[string]interface{} {
	return nil
}

// Add an issue to the index.
func (c *ComicsDB) IndexIssue(issue *comicvine.Issue) error {
	u, err := c.getIndexUpdater()
	if err != nil {
		return err
	}
	return u.Add(&issueIndexDocument{issue})
}

// Adapt a Volume to the IndexDocument interface.
type volumeIndexDocument struct {
	*comicvine.Volume
}

func (d volumeIndexDocument) Fields() map[string]string {
	f := map[string]string{
		"name": d.Name,
	}
	if d.Description != "" {
		f["description"] = util.HTMLToText(d.Description)
	}
	return f
}

func (d volumeIndexDocument) Id() int {
	return d.Volume.Id
}

func (d volumeIndexDocument) Type() string {
	return "volume"
}

func (d volumeIndexDocument) Suggest() map[string]interface{} {
	return map[string]interface{}{
		"input":  d.Name,
		"output": util.RemoveESSpecialChars(d.Name),
		"payload": map[string]interface{}{
			"id": d.Volume.Id,
		},
	}
}

// Add a volume to the index.
func (c *ComicsDB) IndexVolume(vol *comicvine.Volume) error {
	u, err := c.getIndexUpdater()
	if err != nil {
		return err
	}
	return u.Add(&volumeIndexDocument{vol})
}

// Search issues.

func (c *ComicsDB) StartBackgroundJobs() {
	workers := []struct {
		queue    string
		consumer Consumer
	}{
		{"index_issue", &indexIssueConsumer{c}},
		{"index_volume", &indexVolumeConsumer{c}},
		{"incoming", &incomingConsumer{c}},
	}

	for _, params := range workers {
		w, err := NewWorker(params.queue, params.consumer)
		if err != nil {
			log.Fatal(err)
		}
		go w.Run()
	}
}

func (c *ComicsDB) getIndexReader() (*IndexReader, error) {
	// Lazy initialization of the IndexReader.
	err := errors.New("searcher could not be initialized")
	c.readerOnce.Do(func() {
		c.reader, err = NewIndexReader(c.Index)
	})
	if c.reader == nil {
		return nil, err
	}
	return c.reader, nil
}

func (c *ComicsDB) getIndexUpdater() (*IndexUpdater, error) {
	// Lazy initialization of the IndexUpdater.
	err := errors.New("index writer could not be initialized")
	c.updaterOnce.Do(func() {
		c.updater, err = NewIndexUpdater(c.Index)
	})
	if c.updater == nil {
		return nil, err
	}
	return c.updater, nil
}

type incomingFile struct {
	RemotePath string
	LocalPath  string
}

// Consumer for the "incoming" queue. Calls ComicsDB.ProcessFile on
// newly uploaded files.
type incomingConsumer struct {
	*ComicsDB
}

func (c incomingConsumer) Process(payload string) error {
	var inc incomingFile
	if err := json.Unmarshal([]byte(payload), &inc); err != nil {
		return err
	}
	issue, err := c.ProcessFile(&inc)
	if err != nil {
		return err
	}
	log.Printf("processed incoming issue %d", issue.Id)
	EnqueueJSON("index_issue", issue)
	return nil
}

// Consumer for the "index_issue" queue. Adds issues to the index.
type indexIssueConsumer struct {
	*ComicsDB
}

func (c indexIssueConsumer) Process(payload string) error {
	var issue comicvine.Issue
	if err := json.Unmarshal([]byte(payload), &issue); err != nil {
		return err
	}
	log.Printf("indexing issue %d", issue.Id)
	return c.IndexIssue(&issue)
}

// Consumer for the "index_volume" queue. Adds volumes to the index.
type indexVolumeConsumer struct {
	*ComicsDB
}

func (c indexVolumeConsumer) Process(payload string) error {
	var volume comicvine.Volume
	if err := json.Unmarshal([]byte(payload), &volume); err != nil {
		return err
	}
	log.Printf("indexing volume %d", volume.Id)
	return c.IndexVolume(&volume)
}
